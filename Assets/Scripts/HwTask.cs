using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HwTask : MonoBehaviour
{
    int numberInt = 10;
    float numberFloat = 3.14f;
    double numberDouble = 2.71828;
    decimal numberDecimal = 100.50m;
    string resultString;

    [SerializeField] TextMeshProUGUI IntText;
    [SerializeField] TextMeshProUGUI FloatText;
    [SerializeField] TextMeshProUGUI DoubleText;
    [SerializeField] TextMeshProUGUI DecimalText;

    [SerializeField] TMP_InputField IntInput;
    [SerializeField] TMP_InputField FloatInput;
    [SerializeField] TMP_InputField DoubleInput;
    [SerializeField] TMP_InputField DecimalInput;

    void Start()
    {
        UpdateText();
    }

    /// <summary>
    /// ��������� �����
    /// </summary>
    public void UpdateText()
    {
        IntText.text = numberInt.ToString();
        FloatText.text = numberFloat.ToString();
        DoubleText.text = numberDouble.ToString();
        DecimalText.text = numberDecimal.ToString();
    }

    /// <summary>
    /// ����������� ���� ������
    /// </summary>
    public void TypeConversion()
    {
        int convertedInt = (int)numberFloat;
        float convertedFloat = (float)numberDouble;
        double convertedDouble = (double)numberDecimal;
        decimal convertedDecimal = Convert.ToDecimal(numberInt);

        resultString = $"int: {numberInt}, float: {numberFloat}, double: {numberDouble}, decimal: {numberDecimal}\n";
        resultString += $"convertedInt: {convertedInt}, convertedFloat: {convertedFloat}, convertedDouble: {convertedDouble}, convertedDecimal: {convertedDecimal}";
        Debug.Log(resultString);
    }

    /// <summary>
    /// ������ ���� � ���������� � ���� ������
    /// </summary>
    public void WriteAllText()
    {
        File.WriteAllText("data.txt", resultString);
    }

    /// <summary>
    /// �������� ������
    /// </summary>
    /// <param name="type">��� ������</param>
    public void ChangeValues(int type)
    {
        switch (type)
        {
            case 0:
                numberInt = int.Parse(IntInput.text);
                break;
            case 1:
                numberFloat = float.Parse(FloatInput.text);
                break;
            case 2:
                numberDouble = double.Parse(DoubleInput.text);
                break;
            case 3:
                numberDecimal = Convert.ToDecimal(DecimalInput.text);
                break;
        }
        UpdateText();
    }
}
